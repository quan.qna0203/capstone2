import CartProduct from "../models/CartProduct.js";
import Product from "../models/Product.js";

const numberFormat = new Intl.NumberFormat('vi-VN', {
    style: 'currency',
    currency: 'VND',
  });
export let renderProductList = (list) => {
    let listProduct = "";
    list.forEach(item => {
        let contentString = `
        <div class="col-lg-4 col-md-6 col-sm-12 col-xl-3 mt-3">
        <div class="card card-item" style="width: 18rem;">
            <img
                src=${item.imageProduct}
                class="card-img-top"
                style="width: 100%; padding: 15px;" alt="...">
            <div class="card-body text-center">
                <h3 class="card-title"
                    style="font-weight: 800;">${item.productName}</h3>
                <p class="card-text"
                    style="font-weight: 700;
                font-size: 16px;
                line-height: 24px;
                color: #0066CC;">Giá: ${numberFormat.format(item.price)}</p>
                <button onclick="addCart(${item.id})" class="btn btn-primary mt-3 ">Thêm
                    vào giỏ hàng
                </button>
            </div>
        </div>
    </div>
            `;
            listProduct += contentString
    });
    document.querySelector("#productItem").innerHTML=listProduct
}


const BASE_STATUS = true;
export let renderProductCart = (list) => {
    let tblProductCart = "";
    list.forEach(item => {
        let contentString = `
        <tr >
            <td>${item.productName}</td>
            <td>${item.type == "iphone" ? "Iphone" : "SamSung"}</td>
            <td> ${VND.format(item.price)}</td>
            <td>
            <button class="btn btn-primary" onclick="updateQuantity(1,${item.id})">+</button>
            ${item.quantity}
            <button class="btn btn-danger" onclick="updateQuantity(2,${item.id})">-</button>
            </td>
            <td>${item.discount}</td>
            <td>${VND.format(item.giaKhuyenMai)}</td>
            <td>${item.status == BASE_STATUS ? "Còn hàng" : "Hết hàng"}</td>
            <td style="width: 10%;"><img style="width:50%;height:50%" src="${item.imageProduct}"/></td>
            <td>${VND.format(item.price*item.quantity)}</td>
            <td>
            <button class="btn btn-danger" onclick="delProductCart(${item.id})">Xoá</button>
            </td>
        </tr>
            `;
        tblProductCart += contentString
    });
    document.querySelector("#tbodyCartProduct").innerHTML = tblProductCart
}


export let addCartProduct = (data) => {
   let cartProduct = new CartProduct();
   cartProduct.productId = data.id
   cartProduct.productName = data.productName
   cartProduct.type = data.type
   cartProduct.price = data.price
   cartProduct.discount = data.discount
   cartProduct.status = data.status
   cartProduct.imageProduct = data.imageProduct
   cartProduct.giaKhuyenMai = cartProduct.tinhGiaKM()
   cartProduct.description = data.description
   cartProduct.quantity = 1;
   return cartProduct  
}

export let updateCart = (data) => {
console.log(data)
    // data.quantity +=1
    return data  
 }



