


import productServ from '../service/service.js'
import cartServ from '../service/serviceCart.js'
import { addCartProduct, renderProductCart, renderProductList, updateCart } from './index.js';
let fletchProduct = () => {
    productServ.getListProduct().then((res) => {
        renderProductList(res.data)
    })
}
fletchProduct();

let fletchCartProduct = () => {
    // let cart_product_list = localStorage.getItem("cart_product")
    cartServ.getListCartProduct().then((res) => {
        renderProductCart(res.data)
    })
    // renderProductCart(JSON.parse(cart_product_list))
}
window.fletchCartProduct = fletchCartProduct;

let addCart = (id) => {

    cartServ.getListCartProduct().then((res) => {
        let index = res.data.findIndex((resID) => resID.productId == id);
        if (index != -1) {
            res.data[index].quantity += 1;
            cartServ.updateCartProduct(res.data[index]).then(alert("Cập nhật giỏ hàng thành công"))
        } else {
            productServ.getProductById(id).then((res) => {
                cartServ.addCart(addCartProduct(res.data)).then(alert("Thêm thành công"))
            })
        }
    });

}
window.addCart = addCart

let updateQuantity = (params,id)=>{
    if(params==1){
        cartServ.getListCartProduct().then((res) => {
            let index = res.data.findIndex((resID) => resID.id == id);
            res.data[index].quantity += 1;
            cartServ.updateCartProduct(res.data[index]).then(fletchCartProduct)
        })
    }else{
        cartServ.getListCartProduct().then((res) => {
            let index = res.data.findIndex((resID) => resID.id == id);
            res.data[index].quantity -= 1;
            cartServ.updateCartProduct(res.data[index]).then(fletchCartProduct)
        })
    }
}
window.updateQuantity = updateQuantity

let delProductCart =(id)=>{
    cartServ.delProductCart(id).then(fletchCartProduct)
}

window.delProductCart = delProductCart

let findByType = () => {
    let value = document.getElementById("selLoai").value;
    let arrProduct =[]
    productServ.getListProduct().then((res) => {
       res.data.forEach(item => {
            if(item.type ===value){
                arrProduct.push(item)
            }else if(value==="all"){
                arrProduct.push(item)
                return;
            }
       });
       renderProductList(arrProduct)
    })
}

window.findByType = findByType