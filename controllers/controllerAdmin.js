import Product from "../models/Product.js";
const BASE_STATUS = true;
export let renderProductListAdmin = (list) => {
    let tableProduct = "";
    list.forEach(item => {
        let contentString = `
        <tr>
            <td>${item.id}</td>
            <td>${item.productName}</td>
            <td>${item.type == "iphone" ? "Iphone" : "SamSung"}</td>
            <td> ${VND.format(item.price)}</td>
            <td>${item.discount}</td>
            <td>${VND.format(item.giaKhuyenMai)}</td>
            <td>${item.status == BASE_STATUS ? "Còn hàng" : "Hết hàng"}</td>
            <td>
            <button data-toggle="modal" data-target="#exampleModal" class="btn btn-primary" onclick ="editProduct(${item.id})">Chỉnh sửa</button>
            <button data-toggle="modal" class="btn btn-danger" onclick ="delProduct(${item.id})">Xoá</button>
            </td>
         
        </tr>
            `;
        tableProduct += contentString
    });
    document.querySelector("#tbodyProduct").innerHTML = tableProduct
}


export let getInfoProduct = () => {
    let product = new Product();
    product.id = document.querySelector("#id").value
    product.productName = document.querySelector("#productName").value
    product.type = document.querySelector("#type").value
    product.price = document.querySelector("#price").value
    product.discount = document.querySelector("#discount").value
    product.status = document.querySelector("#status").value
    product.imageProduct = document.querySelector("#imageProduct").value
    product.giaKhuyenMai = product.tinhGiaKM()
    product.description = document.querySelector("#description").value
    var valid =
        validation.kiemTraRong(product.productName, "invalidTen", "Tên sản phẩm") & validation.kiemTraRong(product.type, "invalidLoai", "Loại sản phẩm") & validation.kiemTraRong(product.price, "invalidGia", "Giá sản phẩm") & validation.kiemtraTatCaLaSo(product.price, "invalidGiaNumber", "Giá sản phẩm") & validation.kiemTraRong(product.discount, "invalidKM", "Khuyến mãi sản phẩm") & validation.kiemTraRong(product.status, "invalidTT", "Tình trạng sản phẩm") & validation.kiemTraRong(product.imageProduct, "invalidHinhMon", "Hình ảnh sản phẩm") & validation.kiemTraRong(product.description, "invalidMoTa", "Mô tả sản phẩm");
    console.log(valid)
    if (!valid) {
        return;
    }
    return product;
}

export let getEdit = (data) => {
    document.querySelector("#id").value = data.id
    document.querySelector("#productName").value = data.productName
    document.querySelector("#type").value = data.type == "iphone" ? "iphone" : "samsung"
    document.querySelector("#price").value = data.price
    document.querySelector("#discount").value = data.discount
    document.querySelector("#status").value = data.status
    document.querySelector("#imageProduct").value = data.imageProduct
    document.querySelector("#description").value = data.description
}

export let clearForm = () => {
    document.querySelector("#id").value = ""
    document.querySelector("#productName").value = ""
    document.querySelector("#type").value = ""
    document.querySelector("#price").value = ""
    document.querySelector("#discount").value = ""
    document.querySelector("#status").value = ""
    document.querySelector("#imageProduct").value = ""
    document.querySelector("#description").value = ""
}

