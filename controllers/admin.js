
import productServ from "../service/service.js";
import { clearForm, getEdit, getInfoProduct, renderProductListAdmin } from "./controllerAdmin.js";
let fletchProductAdmin = () => {
    productServ.getListProduct().then((res) => {
        renderProductListAdmin(res.data)
    })
}
fletchProductAdmin()

let themSanPham = () => {
    let data = getInfoProduct();
    if (data != null) {
        productServ.addProduct(data).then(fletchProductAdmin);
        clearForm()
    }
}

window.themSanPham = themSanPham

let editProduct = (id) => {
    productServ.getProductById(id).then((res) => {
        getEdit(res.data)
    })
}

window.editProduct = editProduct


let capNhatSanPham = () => {
    let product = getInfoProduct();
    if (product != null) {
        productServ.updateProductById(product).then(fletchProductAdmin)
        clearForm()
    }
}

window.capNhatSanPham = capNhatSanPham

let delProduct = (id) => {
    productServ.delProduct(id).then(fletchProductAdmin)
}

window.delProduct = delProduct

let findByType = () => {
    let value = document.getElementById("selLoai").value;
    let arrProduct =[]
    productServ.getListProduct().then((res) => {
       res.data.forEach(item => {
            if(item.type ===value){
                arrProduct.push(item)
            }else if(value==="all"){
                arrProduct.push(item)
                return;
            }
       });
       renderProductListAdmin(arrProduct)
    })
}

window.findByType = findByType
