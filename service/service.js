const BASE_URL = "https://64eff6058a8b66ecf779042e.mockapi.io"
let getListProduct = () => {
    return axios({
        url: `${BASE_URL}/product`,
        method: "GET"
    })
}

let addProduct = (product)=>{
    return axios({
        url: `${BASE_URL}/product`,
        method: "POST",
        data:product
    })
}

let getProductById = (id)=>{
    return axios({
        url: `${BASE_URL}/product/${id}`,
        method: "GET"
    })
}

let updateProductById = (product)=>{
    return axios({
        url: `${BASE_URL}/product/${product.id}`,
        method: "PUT",
        data:product
    })
}

let delProduct = (id)=>{
    return axios({
        url: `${BASE_URL}/product/${id}`,
        method: "DELETE",
    })
}

let productServ = {
    getListProduct,
    addProduct,
    getProductById,
    updateProductById,
    delProduct
}



export default productServ