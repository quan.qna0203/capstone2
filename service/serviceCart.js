const BASE_URL = "https://64eff6058a8b66ecf779042e.mockapi.io"
let addCart = (product)=>{
    return axios({
        url: `${BASE_URL}/productCart`,
        method: "POST",
        data:product
    })
}

let getListCartProduct = () => {
    return axios({
        url: `${BASE_URL}/productCart`,
        method: "GET"
    })
}

let updateCartProduct = (product) => {
    return axios({
        url: `${BASE_URL}/productCart/${product.id}`,
        method: "PUT",
        data:product
    })
}

let delProductCart = (id)=>{
    return axios({
        url: `${BASE_URL}/productCart/${id}`,
        method: "DELETE",
    })
}
let cartServ = {
    addCart,
    getListCartProduct,
    updateCartProduct,
    delProductCart
    
}

export default cartServ