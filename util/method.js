var validation = {
  kiemTraRong: function (value, name, text) {
    if (value.trim() === "") {
      document.querySelector(`#${name}`).style="display:block"
      document.querySelector(`#${name}`).innerHTML = `<p> ${text} không được bỏ trống!</p>`
      return false;
    }
    document.querySelector(`#${name}`).innerHTML = "";
    return true;
  },
  kiemtraTatCaLaSo:function (value,name,text){
    var regex = /[0-9]+/;
    if (regex.test(value)) {
      document.querySelector(`#${name}`).innerHTML = "";
      return true;
    }
    document.querySelector(`#${name}`).style="display:block"
    document.querySelector(`#${name}`).innerHTML = `<p> ${text} phải là số</p>`
    return false;
  },
  kiemTraTatCaKyTu: function (value, name, text) {
    var regexLetter = /^[a-zA-Z!@#\$%\^\&*\)\(+=._-]{2,}$/g;
    if (regexLetter.test(stringToSlug(value))) {
      document.querySelector(`#${name}`).innerHTML = "";
      return true;
    }
    document.querySelector(`#${name}`).style="display:block"
    document.querySelector(`#${name}`).innerHTML = `<p> ${text} phải là ký tự</p>`
    return false;
  },
  kiemTraEmail: function (value, name) {
    var regexEmail =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (regexEmail.test(value)) {
      document.querySelector(`#${name}`).innerHTML = "";
      return true;
    }
    document.querySelector(`#${name}`).innerHTML = "Email không đúng định dạng";
    return false;
  },
  kiemTraMatKhau: function (value, name) {
    var regexNumber =
      /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{6,10}$/;
    if (regexNumber.test(value)) {
      document.querySelector(`#${name}`).innerHTML = "";
      return true;
    }
    document.querySelector(`#${name}`).innerHTML =
      "Mật khẩu phải từ 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)";
    return false;
  },
  kiemTraNgayThang: function (value, name) {
    var regexNgayThangNam =
      /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/;
    if (regexNgayThangNam.test(value)) {
      document.querySelector(`#${name}`).innerHTML = "";
      return true;
    }
    document.querySelector(`#${name}`).innerHTML =
      "Ngày tháng năm không đúng định dạng tháng/ngày/năm";
    return false;
  },
  kiemTraDoDai: function (value, name, text, minLength, maxLength) {
    var length = value.length;
    if (length < minLength || length > maxLength) {
      document.querySelector(
        `#${name}`
      ).innerHTML = ` ${text} từ ${minLength} - ${maxLength} ký tự!!`;
      return false;
    }
    document.querySelector(`#${name}`).innerHTML = "";
    return true;
  },
  kiemTraGiaTri: function (value, name, minValue, maxValue, text) {
    if (Number(value) < minValue || Number(value) > maxValue) {
      document.querySelector(`#${name}`).innerHTML = text;
      return false;
    }
    document.querySelector(`#${name}`).innerHTML = "";
    return true;
  },
};

const VND = new Intl.NumberFormat("vi-VN", {
  style: "currency",
  currency: "VND",
});
function stringToSlug(title) {
  //Đổi chữ hoa thành chữ thường
  slug = title.toLowerCase();
  //Đổi ký tự có dấu thành không dấu
  slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, "a");
  slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, "e");
  slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, "i");
  slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, "o");
  slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, "u");
  slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, "y");
  slug = slug.replace(/đ/gi, "d");
  //Xóa các ký tự đặt biệt
  slug = slug.replace(
    /\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi,
    ""
  );
  //Đổi khoảng trắng thành ký tự gạch ngang
  slug = slug.replace(/ /gi, "-");
  //Đổi nhiều ký tự gạch ngang liên tiếp thành 1 ký tự gạch ngang
  //Phòng trường hợp người nhập vào quá nhiều ký tự trắng
  slug = slug.replace(/\-\-\-\-\-/gi, "-");
  slug = slug.replace(/\-\-\-\-/gi, "-");
  slug = slug.replace(/\-\-\-/gi, "-");
  slug = slug.replace(/\-\-/gi, "-");
  //Xóa các ký tự gạch ngang ở đầu và cuối
  slug = "@" + slug + "@";
  slug = slug.replace(/\@\-|\-\@|\@/gi, "");

  return slug;
}
