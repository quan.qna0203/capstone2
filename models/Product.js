export default class Product{
    constructor(id,productName,type,price,discount,status,imageProduct,description){
        this.id =id;
        this.productName = productName;
        this.type = type;
        this.price = price;
        this.discount = discount;
        this.status = status;
        this.imageProduct = imageProduct;
        this.description = description
    }
    tinhGiaKM = function (){
        return this.price*(1-this.discount/100)
    }
}