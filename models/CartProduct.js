export default class CartProduct {
    constructor(id, productName, type, price, discount, status, imageProduct, description, quantity) {
        this.id = id;
        this.productName = productName;
        this.type = type;
        this.price = price;
        this.discount = discount;
        this.status = status;
        this.imageProduct = imageProduct;
        this.description = description;
        this.quantity = quantity
    }
    tinhGiaKM = function () {
        return this.price * (1 - this.discount / 100)
    }
}